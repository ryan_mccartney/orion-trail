#Orion Trail System Specifications - Software#

The following software is required for the operation of the Orion Trail Application, including debugging and editing source code:

**Debugging & Code Editing**
--
 - Visual Studio Community 2013 or later
 - Windows 7 or later (Recommend Windows 8.1)
 - XNA 4.0 Studio (Including XACT 3.0)
 - Git (Git Bash) and/or Atlassian SourceTree

**Software Prerequisites**
--
*These are required to operate XNA and Visual Studio, and most of them are either installed by default with Windows or will be downloaded and installed when Visual Studio & XNA are installed.*

- .Net Framework
- .Net Core Framework
- C++ & Visual C++ Runtime
