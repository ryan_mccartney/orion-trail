#Orion Trail System Specifications - Hardware#

The following hardware is required for the operation of the Orion Trail Application:

**Minimum Requirements:**
--
- [Intel Pentium E2180 2.0Ghz](http://ark.intel.com/products/31733/Intel-Pentium-Processor-E2180-1M-Cache-2_00-GHz-800-MHz-FSB) or higher processor
- Intel HD Graphics 4000
- 1024MB RAM
- 1024 x 768 Resolution Display
- At least 2GB of free storage space on HDD or SSD
- Keyboard & Mouse
- Speakers

**Recommended Requirements:**
--
- Intel Core i5 2.5Ghz or higher processor
- Nvidia GeForce 8800/AMD Radeon HD 5670 or equivalent video card (Dedicated GPU with 512MB+ Video Memory(VRAM))
- 2048MB RAM
- 1920 x 1080 Resolution Display
- At least 2GB of free storage space on HDD or SSD
- Keyboard & Mouse
- Speakers / Headphones

**Why I chose these Specifications**
--
XNA is pretty hardware independent and allows for a wide range of hardware specifications to be used. [Terraria](https://terraria.org/) is a popular 2D adventure game developed in XNA & MonoGame. I used Terraria as a guide for these specifications, as both my application and Terraria share similar aspects and hardware requirements.
