#Orion Trail - User Manual#

Orion Trail is a (mostly) top-down action/arcade game in which you (the player) embark on a journey across Orion's belt! Get in your ship and charge the lasers, it's going to get bumpy!

**Installation - Source code and Debugging**
--
1. Clone the repo (https://ryan_mccartney@bitbucket.org/ryan_mccartney/orion-trail.git) locally using SourceTree or Git.

![Cloning the repository in Git](gitClone_UM.png)
\pagebreak

2. Once the repo is cloned, unzip the *XNA Game Studio 4.0.4* file which is located in the *Installation File Requirements* folder.

![Installation File Requirements Folder](installFileLocation_UM.png)

3. Follow the readme inside the unzipped folder to know what order to install the framework's dependencies in. Ensure that you have all the necessary Microsoft .Net frameworks installed (should be installed by default with Windows 7 or later, and may be installed via the XNA Installer if missing from your PC).

4. Once installed, open the Visual Studio file *Orion Trail_McCartney* in order to see the VS project.

\pagebreak

5. In the solution explorer, right click on *references*, which will be nested under the *Orion Trail_McCartney* list option (**NOT UNDER THE CONTENT/CONTENT PIPELINE SECTION**).

![References in Solution](referencesLocation_UM.png)

6. Select *Add Reference*.

7. Select *Browse* and find the *RuminateGUI.dll* which is included in the *Installation File Requirements* folder of the *Orion Trail_McCartney* repo. Select the *RuminateGUI.dll* file, and then check the checkbox next to the *RuminateGUI.dll* option in the list. Click *OK*.
![Browse References](browseReferences_UM.png)

8. You may also need to add the *Microsoft.Xna.Framework.Xact* reference. XACT is the format used for all the game's audio, and will be needed for debugging. It should be there by default when you open the project, but double check :)

8. Ensure that there are no errors in the *Microsoft.XNA.Framework*, *Microsoft.Xna.Framework.Xact* and *RuminateGUI* references within the solution's references.

![Check yo reference!](checkReferences_UM.png)

\pagebreak

10. If there are any issues debugging (ie. Debugging won't start), ensure that the project's startup project is *OrionTrail_McCartney* **NOT** *OrionTrail_McCartney(Content)*. This can be changed by selecting *Project > Properties > Startup Project*.

![Startup Project](startupProject_UM.png)
\pagebreak

11. As a precaution (in case of lovely graphical errors!) go to the fonts folder: *Resources > Fonts*. Install any fonts located in this folder to your machine. This may not be needed due to the use of Spritefonts within the project, however if you notice any errors in text not being displayed, this will solve it.

12. Happy marking/debugging!

![Happy Debugging!](debugScreen_UM.png)

**Source files can be found in the folder *Installation File Requirements* inside the *Orion Trail_McCartney* repository folder.**

\pagebreak

**Installation - Compiled Executable**
--
1. Drag the folder containing the executable and all its files to a location of your choosing.

![Eg. Drag to /My Documents/My Games](executableLocation_UM.png)

2. Open the *Orion Trail* or similarly named *.exe* file.

![Open the .exe and enjoy!](applicationLocation_UM.png)

3. Enjoy!

![Enjoy Playing!](playingGame_UM.png)

\pagebreak

**Playing The Game - How To**
--
*Orion Trail* is an easy to learn game with simple controls but in-depth features.

**Interaction** with all menus is done via the mouse. Simply point and click. Many of the UI elements (ie. buttons) will flash or slightly bezel when the mouse is hovering over them.

The screen-shots below show a button without a mouse hovering on it and a button with a mouse hovering on it. While the mouse remains hovering on the button, the white overlay will slowly disappear and reappear, creating a flashing effect.

![Button Without Mouse Hover](buttonNoFlash.png)
![Button with mouse hover](buttonFlash.png)
\pagebreak


- **Movement** in the game is done via the arrow-keys. When controlling the purple player ship, **UP** & **DOWN** move the player forwards and backwards, while **LEFT** & **RIGHT** rotate the ship left and right.

- **Combat** is activated by firing your ship's lasers with the **SPACEBAR**.

- **Exiting menus** can be done with the **ESCAPE** key.

**Abbreviated List of Controls**
--
- Menu Interaction: **MOUSE**
- Movement: **ARROW-KEYS**
- Combat/Fire Lasers: **SPACEBAR**
- Exit Menu: **ESC**

**Objective of the Game - Wait...What Am I Doing?**
--
Traveling through Orion's Belt is a momentous task, and there'll be plenty of enemy ships along the way to cause havoc. For some reason, they mostly decided that lasers are obsolete and will rely on their complex navigation systems to bring you down!

Being attacked by an enemy ship will cause some hull damage, reach 0 hull and you'll be destroyed!

When a group of enemies attack you, their onslaught will be seemingly relentless! Survive long enough and you'll advance a level, but their attacks will grow even more relentless(er!).

**Survival Mode** can be activated by clicking the checkbox at the main menu.

![Survival Mode](survivalMode_UM.png)

Activating **survival mode** and then clicking the **Play** button will start survival mode, in which you must simply try and survive for as long as you can. High scores are saved locally to *MyDocuments* and the *SavedGames* folder. This is dynamic and will automatically create a folder and save to this location on your PC.

To view your best time, click the **High Scores** button at the main menu, located at the bottom-right corner of the screen.

\pagebreak

**Additional Troubleshooting**
--
Common errors that can occur during debugging & testing, as well as during the playing of the game along with solutions for said errors can be found here. Some of the more likely debugging errors (that will be encountered early) are covered in the initial install instructions within this manual.

**Debugging Troubleshooting**

  - "*Visual Studio Cannot Open This Solution*" or other error messages along these lines when first opening the *.sln* file for the Visual Studio solution is due to not having XNA installed. Refer back to the install instructions for debugging in this manual for instructions as to installing XNA and getting the solution (and its references) working.

  - When first setting up the solution for debugging, if you have a variety of syntax errors (of which there are none, I assure you) this is due to not having a particular reference. Again, refer to the installation instructions at the start of this manual for further information, although the most likely culprit is the RuminateGUI reference. Files for this reference are found in the *Installation File Requirements* folder in the base repository for *Orion Trail_McCartney*.
  \pagebreak

**Playing the Game - Common Issue Troubleshooting**

  - Highscores updating -> Highscores will only save (as only your best is displayed) if your survival mode score is better than your previous. You can view your save files in /MyDocuments/SavedGames/Orion-Trail/

  - Lasers firing out of player ship slightly off-centre / enemy ships hit-boxes being slightly smaller than what it looks like it should be. This is simply an art problem that is particularly finicky to solve and would require a lot of testing and graphics editing (which is not worth the length of time a one man development team has available). For the lasers firing off centre, it is due to the way XNA handles the texture an image has. While facing one way, the 'rectangle' object that is the player has its default texture position (this is facing 090"T). When facing another direction, this texture has to adjust and so the centre of this texture (the place the laser fires from) is slightly different, causing off-centre laser shots. The issue of enemy hit-boxes is due to mostly the same reason, with the bounds of the texture being (quite strangely) slightly off when the rectangle is facing a particular direction. This causes some interesting issues with enemies seemingly dodging lasers, but there just isn't the time to fix this issue (especially as it isn't exactly gamebreaking).

  - Window resizing -> In order to stop a myriad of horrible UI errors from destroying the game, the window cannot be resized. You can resize the edges but it will simply scale everything back up and return to its original resolution. You may however 'maximize' the window by clicking the Windows default button (a square) in the top-right corner of the window. Everything in the window will scale up to this new resolution, but be warned, as there may be issues when using the mouse to interact with UI elements in a highly scaled up resolution - Simply scale it back down to the default resolution to solve these UI issues.
    - Note: The scaling is in place so that an accidental maximization of the window doesn't cause the game to crash. It is not meant to be played in a high/fullscreen resolution.
