# README - Orion Trail #

Orion Trail - A Quest Through Constellations

Built in C# using the XNA Framework and the Visual Studio Community IDE.

**Project by Ryan McCartney.**

### What is this repository for? ###

* This is Ryan McCartney's Major Work for HSC Software Design and Development 2015

### How do I get set up? ###

1. Clone the repo locally using SourceTree or Git.

2. Once the repo is cloned, unzip the *XNA Game Studio 4.0.4* file which is located in the *Installation File Requirements* folder.

3. Follow the readme inside the unzipped folder to know what order to install the framework's dependencies in. Ensure that you have all the necessary Microsoft .Net frameworks installed (should be installed by default with Windows 7 or later, and may be installed via the XNA Installer if missing from your PC).

4. Once installed, open the Visual Studio file *Orion Trail_McCartney* in order to see the VS project.

5. In the solution explorer, right click on *references*, which will be nested under the *Orion Trail_McCartney* list option (**NOT UNDER THE CONTENT/CONTENT PIPELINE SECTION**).

6. Select *Add Reference*.

7. Select *Browse* and find the *RuminateGUI.dll* which is included in the *Installation File Requirements* folder of the *Orion Trail_McCartney* repo. Select the *RuminateGUI.dll* file, and then check the checkbox next to the *RuminateGUI.dll* option in the list. Click *OK*.

8. You may also need to add the *Microsoft.Xna.Framework.Xact* reference. XACT is the format used for all the game's audio, and will be needed for debugging. It should be there by default when you open the project, but double check :)

8. Ensure that there are no errors in the *Microsoft.XNA.Framework*, *Microsoft.Xna.Framework.Xact* and *RuminateGUI* references within the solution's references.

9. If there are any issues debugging (ie. Debugging won't start), ensure that the project's startup project is *OrionTrail_McCartney* **NOT** *OrionTrail_McCartney(Content)*. This can be changed by selecting *Project > Properties > Startup Project*.

9. As a precaution (in case of lovely graphical errors!) go to the fonts folder: *Resources > Fonts*. Install any fonts located in this folder to your machine. This may not be needed due to the use of Spritefonts within the project, however if you notice any errors in text not being displayed, this will solve it.

9. Happy marking/debugging!

**Source files can be found in the folder *Installation File Requirements* inside the *Orion Trail_McCartney* repository folder.**

### Python/Bash Auto-blogger ###

There are two files located in the main folder for the repository - *Autogitlog.sh* and *autoPostWordpress.py*. The Python file uses the shell script to create a text file which contains the commit summary of the most recent Git commit. This file is stored in the same location as the *Autogitlog.sh* under the name *log.txt*. The Python file then reads in the necessary info it needs from the text file, and posts that information onto the student [SDD Blogs](cs.tsc.nsw.edu.au/students). This was to cut out the 'middle-man' so to speak, as commit messages were being done anyway, and having to re-do the same message on the blog was just a frustration.

The Python plugin for posting on the blog was *wordpress_xmlrpc*. Documentation can be found [here](https://python-wordpress-xmlrpc.readthedocs.org/en/latest/).

### Who do I talk to? ###

* Ryan McCartney
  * ryan.mccartney@student.tsc.nsw.edu.au
