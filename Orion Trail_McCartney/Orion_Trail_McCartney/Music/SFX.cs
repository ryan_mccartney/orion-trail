﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using Orion_Trail_McCartney.Main;

namespace Orion_Trail_McCartney.Music
{
     static class Sfx
     {
         // The audio is being handled through the XNA XACT pipeline.
         // This involved me putting all my audio into the XACT application and manually
         // tagging each audio clip with a tag and a 'cue'. This class as well as Soundtrack.cs both handle
         // the XACT audio clips which contain all the sfx and all the soundtracks and their respective cues.
         // This allows for multiple sounds to be played at once quite efficiently. The XACT audio files at the .xap and
         // .xgs files in the 'Audio' folder in this solution's content folder.

         public const int NumberOfLaserTracks = 9;

         private static SoundBank _sounds;
         private static Cue _currentTrack;

         public static int CurrentTrack;
         public static bool EnableSfx;

         static Sfx()
         {
             _sounds = OrionTrailGame.Sounds;
             EnableSfx = true;
         }

         public static void Initialize()
         {
             // Setting this class to play only tracks tagged as 'SFX'
             OrionTrailGame.SfxCategory = OrionTrailGame.Audio.GetCategory("SFX");
         }

         public static void PlayLaser()
         {
             int track;

             do
             {
                 track = OrionTrailGame.Random.Next(1, NumberOfLaserTracks);

             } while (track == CurrentTrack && NumberOfLaserTracks > 1);

             _currentTrack = _sounds.GetCue("laser" + track.ToString());
             CurrentTrack = track;
             _currentTrack.Play();
         }

         public static void PlayExplosions()
         {
             int track;

             do
             {
                 track = OrionTrailGame.Random.Next(1, 4);
             } while (track == CurrentTrack && NumberOfLaserTracks > 1);

             _currentTrack = _sounds.GetCue("explosion" + track.ToString());
             CurrentTrack = track;
             _currentTrack.Play();
         }

         public static void StopTracks()
         {

            _currentTrack.Stop(AudioStopOptions.AsAuthored);
            _currentTrack.Dispose();
            _currentTrack = null;

         }
     }
}
