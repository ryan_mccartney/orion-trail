﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using Orion_Trail_McCartney.Main;

namespace Orion_Trail_McCartney.Music
{
    static class SoundTrack
    {
        // The audio is being handled through the XNA XACT pipeline.
        // This involved me putting all my audio into the XACT application and manually
        // tagging each audio clip with a tag and a 'cue'. This class as well as Sfx.cs both handle
        // the XACT audio clips which contain all the sfx and all the soundtracks and their respective cues.
        // This allows for multiple sounds to be played at once quite efficiently. The XACT audio files at the .xap and
        // .xgs files in the 'Audio' folder in this solution's content folder.

        public const int NumberOfTracks = 14;

        private static SoundBank _sounds;
        private static Cue _currentTrack;

        public static int CurrentTrack;
        public static bool EnableSoundTrack;

        static SoundTrack()
        {
            _sounds = OrionTrailGame.Sounds;
            EnableSoundTrack = true;

        }

        public static void Initialize()
        {
            _currentTrack = _sounds.GetCue("mainMenuMusic");

            // Setting this class to play only tracks tagged as 'Default' (All the music tracks)
            OrionTrailGame.MusicCategory = OrionTrailGame.Audio.GetCategory("Default");

            StartTrack();
        }

        public static void Update()
        {
            if (EnableSoundTrack)
            {
                if ((_currentTrack == null))
                    StartTrack();
                else if (!_currentTrack.IsPlaying)
                {
                    StopTrack();
                    StartTrack();
                }
            }
            else
            {
                {
                    if (_currentTrack != null)
                        StopTrack();
                }
            }
        }

        public static void StartTrack()
        {
            int track;

            // Checks that nothing is playing already
            if (_currentTrack != null && _currentTrack.IsPlaying)
                StopTrack();

            do
            {
                track = OrionTrailGame.Random.Next(NumberOfTracks);

            } while (track == CurrentTrack && NumberOfTracks > 1);

            _currentTrack = _sounds.GetCue("mainMenuMusic");
            CurrentTrack = track;
            _currentTrack.Play();

        }

        
        public static void ResetNewTrack(int track)
        {
            if (_currentTrack != null && _currentTrack.IsPlaying)
                StopTrack();

            _currentTrack = _sounds.GetCue("mainMenuMusic");
            CurrentTrack = track;
            _currentTrack.Play();
            
        }

        public static void StopTrack()
        {
            _currentTrack.Stop(AudioStopOptions.AsAuthored);
            _currentTrack.Dispose();
            _currentTrack = null;
        }

    }
}
