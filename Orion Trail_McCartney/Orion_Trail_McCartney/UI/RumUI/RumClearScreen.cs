﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Orion_Trail_McCartney.Main;
using Ruminate.GUI.Content;
using Ruminate.GUI.Framework;

namespace Orion_Trail_McCartney.UI.RumUI
{
    public class RumClearScreen : Screen
    {
        private Gui _gui;
        private GraphicsDevice _graphics;

        public override void Init(OrionTrailGame game, GraphicsDevice graphics)
        {
            var skin = new Skin(game.GreyImageMap, game.GreyMap);
            var text = new Text(game.GreySpriteFont, Color.Black);

            var testSkin = new Skin(game.RumImageMap, game.RumMap);
            var testText = new Text(game.RumSpriteFont, Color.Black);

            var testSkins = new[] { new Tuple<string, Skin>("testSkin", testSkin) };
            var testTexts = new[] { new Tuple<string, Text>("testText", testText) };

            this._graphics = graphics;

            _gui = new Gui(game, skin, text, testSkins, testTexts)
            {
                /*THIS IS PURPOSELY EMPTY IN ORDER TO ACT AS AN EMPTY RUMUI
                 * THIS FORM WILL ACT AS A METHOD TO CLEAR THE RUM UI */
            };
        }

        public override void OnResize()
        {
            _gui.Resize();
        }

        public override void Update()
        {
            _gui.Update();
        }

        public override void Draw()
        {
            _gui.Draw();
        }
    }
}
