﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Windows.Forms.VisualStyles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Orion_Trail_McCartney.Main;
using Orion_Trail_McCartney.Music;
using Ruminate.GUI.Content;
using Ruminate.GUI.Framework;

namespace Orion_Trail_McCartney.UI.RumUI
{
    public class RumDeathScreen : Screen
    {

        // RUM UI REQUIREMENTS
        private Gui _gui;
        private GraphicsDevice _graphics;

        // RUM UI ELEMENT REQUIREMENTS
        private Label deathLabel;
        // Framework assignment due to conflict with custom Button.cs
        private Ruminate.GUI.Content.Button continueButton;
        private Ruminate.GUI.Content.Button quitButton;

        public override void Init(OrionTrailGame game, GraphicsDevice graphics)
        {
            /* The following are the two different skins and fonts that RumUI will use.
             * Skin and Text are the default, with different spriteFonts.
             * TestSkin and TestText are the black font version, without bold. 
             * The Variable names are named as such so that it fits with the default for RumUI
             */

            var skin = new Skin(game.GreyImageMap, game.GreyMap);
            var text = new Text(game.GreySpriteFont, new Color(222, 238, 214));

            var testSkin = new Skin(game.RumImageMap, game.RumMap);
            var testText = new Text(game.RumSpriteFont, Color.Black);

            var testSkins = new[] { new Tuple<string, Skin>("testSkin", testSkin) };
            var testTexts = new[] { new Tuple<string, Text>("testText", testText) };

            this._graphics = graphics;

            _gui = new Gui(game, skin, text, testSkins, testTexts)
            {
                Widgets = new Widget[]
                {
                    // Constant use here is due to width of string "GAME OVER MAN!" remaining constant as well as location remaining static
                    deathLabel = new Label((_graphics.Viewport.Bounds.Width / 2) - 80 , _graphics.Viewport.Bounds.Height / 2, "GAME OVER MAN!"), 
                
                    continueButton = new Ruminate.GUI.Content.Button((_graphics.Viewport.Bounds.Width / 2) - 50, (_graphics.Viewport.Bounds.Height / 2) + 40, "Continue?", buttonEvent:
                        delegate(Widget widget)
                        {
                            OrionTrailGame.CurrentGameState = OrionTrailGame.GameState.AfterDeath;
                        }),

                    quitButton = new Ruminate.GUI.Content.Button((_graphics.Viewport.Bounds.Width / 2) - 50, (_graphics.Viewport.Bounds.Height / 2) + 75, "Quit.", buttonEvent:
                        delegate(Widget widget)
                        {
                            // Call Quit Gamestate to end runtime
                            OrionTrailGame.CurrentGameState = OrionTrailGame.GameState.Quit;
                        }),
                    
                
                }
            };
        }

        public override void OnResize()
        {
            _gui.Resize();
        }

        public override void Update()
        {
            _gui.Update();
        }

        public override void Draw()
        {
            _gui.Draw();
        }

    }
}
