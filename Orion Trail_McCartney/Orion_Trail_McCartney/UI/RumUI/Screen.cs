﻿/* -----------RUMUI------------ /
 * The following code is using the RuminateGUI UI Framework
 * http://xnagui.codeplex.com/documentation
 * ------------------------- /
*/

#region References
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Orion_Trail_McCartney.Main;

#endregion

namespace Orion_Trail_McCartney.UI.RumUI
{
    public abstract class Screen
    {
        public Color Color { get; set; }

        public abstract void Init(OrionTrailGame game, GraphicsDevice graphics);
        public abstract void OnResize();
        public abstract void Update();
        public abstract void Draw();
    }
    
}
