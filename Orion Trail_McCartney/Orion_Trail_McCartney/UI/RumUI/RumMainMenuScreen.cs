﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Windows.Forms.VisualStyles;
using System.Xml;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Orion_Trail_McCartney.Main;
using Ruminate.GUI.Content;
using Ruminate.GUI.Framework;

namespace Orion_Trail_McCartney.UI.RumUI
{
    public class RumMainMenuScreen : Screen
    {
        // RUM UI REQUIREMENTS
        private Gui _gui;
        private GraphicsDevice _graphics;

        // RUM UI ELEMENT REQUIREMENTS
        private Label _titleLabel;
        public static CheckBox _survivalModeCheckBox;
        private Ruminate.GUI.Content.Button _highScoresButton;
        private Ruminate.GUI.Content.Button _exitHighScoresButton;
        private Label _currentScoresLabel;

        public override void Init(OrionTrailGame game, GraphicsDevice graphics)
        {
            /* The following are the two different skins and fonts that RumUI will use.
             * Skin and Text are the default, with different spriteFonts.
             * TestSkin and TestText are the black font version, without bold. 
             * The Variable names are named as such so that it fits with the default for RumUI
             */

            var skin = new Skin(game.GreyImageMap, game.GreyMap);
            var text = new Text(game.GreySpriteFont, new Color(222, 238, 214));

            var testSkin = new Skin(game.RumImageMap, game.RumMap);
            var testText = new Text(game.RumSpriteFont, Color.Black);

            var testSkins = new[] { new Tuple<string, Skin>("testSkin", testSkin) };
            var testTexts = new[] { new Tuple<string, Text>("testText", testText) };

            this._graphics = graphics;

            _gui = new Gui(game, skin, text, testSkins, testTexts)
            {
                Widgets = new Widget[]
                {
                    _titleLabel = new Label((_graphics.Viewport.Bounds.Width / 2) - 80, (_graphics.Viewport.Bounds.Height / 2) - 180, "Orion Trail"),

                    _survivalModeCheckBox = new CheckBox(_graphics.Viewport.Bounds.Width - 250, _graphics.Viewport.Bounds.Height - 35, "Survival Mode"),

                    _exitHighScoresButton = new Ruminate.GUI.Content.Button((_graphics.Viewport.Bounds.Width / 2) - 35, _graphics.Viewport.Bounds.Height - 150, "Exit", buttonEvent:
                        delegate(Widget widget)
                        {
                            OrionTrailGame.CurrentGameState = OrionTrailGame.GameState.MainMenu;
                            _survivalModeCheckBox.Visible = true;
                            _titleLabel.Visible = true;
                            _exitHighScoresButton.Visible = false;
                            _highScoresButton.Visible = true;
                            _currentScoresLabel.Visible = false;
                        }) {Visible = false},

                    _highScoresButton = new Ruminate.GUI.Content.Button(0, (_graphics.Viewport.Bounds.Height - 35), "High Scores", buttonEvent:
                        delegate (Widget widget)
                        {
                            OrionTrailGame.CurrentGameState = OrionTrailGame.GameState.HighScores;
                            _survivalModeCheckBox.Visible = false;
                            _titleLabel.Visible = false;
                            _exitHighScoresButton.Visible = true;
                            _highScoresButton.Visible = false;
                            _currentScoresLabel.Visible = true;

                        }),

                    _currentScoresLabel = new Label((_graphics.Viewport.Bounds.Width / 2) - 190, (_graphics.Viewport.Bounds.Height / 2) - 180, "Survival Mode - Best Time:") {Visible = false},

                }
                
            };
        }

        public override void OnResize()
        {
            _gui.Resize();
        }

        public override void Update()
        {
            _gui.Update();
        }

        public override void Draw()
        {
            _gui.Draw();
        }

    }
}
