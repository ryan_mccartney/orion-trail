﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Windows.Forms.VisualStyles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Orion_Trail_McCartney.Main;
using Orion_Trail_McCartney.Music;
using Ruminate.GUI.Content;
using Ruminate.GUI.Framework;

namespace Orion_Trail_McCartney.UI.RumUI
{
    public class RumOptionsMenu : Screen
    {
        // RUM UI REQUIREMENTS
        private Gui _gui;
        private GraphicsDevice _graphics;

        // RUM UI ELEMENT REQUIREMENTS
        public static Slider VolSlider;
        private Label _volSliderLabel;
        public static float CurrentVolValue = 1;
        private Label _helpLabel;
        private Ruminate.GUI.Content.Button _returnButton;
        

        public override void Init(OrionTrailGame game, GraphicsDevice graphics)
        {
            /* The following are the two different skins and fonts that RumUI will use.
             * Skin and Text are the default, with different spriteFonts.
             * TestSkin and TestText are the black font version, without bold. 
             * The Variable names are named as such so that it fits with the default for RumUI
             */
            
            
            var skin = new Skin(game.GreyImageMap, game.GreyMap);
            var text = new Text(game.GreySpriteFont, new Color(222, 238, 214));

            var testSkin = new Skin(game.RumImageMap, game.RumMap);
            var testText = new Text(game.RumSpriteFont, Color.Black);

            var testSkins = new[] { new Tuple<string, Skin>("testSkin", testSkin) };
            var testTexts = new[] { new Tuple<string, Text>("testText", testText) };

            this._graphics = graphics;

           
            _gui = new Gui(game, skin, text, testSkins, testTexts)
            {
                
                Widgets = new Widget[]
                {
                    /* Title Banner */
                    new Image(10,55,240,80, game.RumImages[0]),
                    new Label(85, 80, "Options"),

                    /* Volume Slider */
                    VolSlider = new Slider(100, 200, 200, delegate(Widget slider)
                    {
                        /* All the events that occur happen on sliderValue change.
                         * Label Text updates and rounds off to an integer.
                         * Current values are updated for saving (so that it doesn't reset values when
                         * the menu changes.
                         * Audio Category volumes are updated to be equal with slider value */

                        _volSliderLabel.Value = "Volume: " + ((((Slider) slider).Value)*100).ToString("####");
                        VolSlider.Value = ((Slider) slider).Value;
                        CurrentVolValue = VolSlider.Value;
                        game.OptionsSettingsChanged = true;
                        OrionTrailGame.MusicCategory.SetVolume(VolSlider.Value);
                        OrionTrailGame.SfxCategory.SetVolume(VolSlider.Value);
                        

                    }),
                    _volSliderLabel = new Label(105,165, "Volume: " + (CurrentVolValue * 100).ToString("####")),
                    // Updating the label for the slider

                    /* Help Label and Exit Button */
                    _returnButton = new Ruminate.GUI.Content.Button((_graphics.Viewport.Bounds.Width / 2) - 35, _graphics.Viewport.Bounds.Height - 150, "Exit", buttonEvent:
                        delegate(Widget widget)
                        {
                            OrionTrailGame.CurrentGameState = OrionTrailGame.GameState.MainMenu;
                        }),

                    // Image holder for label
                    new Image(405, 55, 240, 80, game.RumImages[0]),

                    // Label, \n is newline symbol
                    _helpLabel = new Label(470, 80, "Controls \n \n 'ARROW KEYS' -> Movement of Ship \n 'SPACEBAR' -> Fire Laser \n 'ESC' -> Return to previous menu \n 'MOUSE' -> Menu Interaction"),

                 
                }
            };
        }

        public override void OnResize()
        {
            _gui.Resize();
        }

        public override void Update()
        {
            _gui.Update();
        }

        public override void Draw()
        {
            _gui.Draw();
        }

        public static float SetSliderValues(float val)
        {
            VolSlider.Value = val;
            return val;
        }

        internal static void SetSliderValues()
        {
            // Throw new exception incase the slider has no value
            throw new NotImplementedException();
        }

     
    }

    
}
