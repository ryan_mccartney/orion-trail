﻿#region References

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

#endregion

 // For each new added button, must add:
 // <buttonName>.Update(mouse) when button is to be visible  
 // This is done in the Update(gameTime gametime) section of OrionTrailGame.cs

namespace Orion_Trail_McCartney.UI
{
    internal class Button
    {
        #region Variable Declarations
        private readonly Texture2D _buttonTexture;
        private Vector2 _position;
        //
        #region Separate Button Declarations
        private Rectangle _enterGameBtn;
        private Rectangle _optionsBtn;
        private Rectangle _quitBtn;
        #endregion
        //
        private Color _color = new Color(255, 255, 255, 255);
        public Vector2 Size;
        private bool _down;
        public bool IsClicked;
        #endregion

        public Button(Texture2D newTexture)
        {
            _buttonTexture = newTexture;
            //Size = new Vector2(GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width/8,
            //    GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height/30);
            Size = new Vector2(250, 80);
        }

        public void Update(MouseState mouse)
        {

            _enterGameBtn = new Rectangle((int)_position.X, (int)_position.Y,
                (int)Size.X, (int)Size.Y);

            _optionsBtn = new Rectangle((int)_position.X, (int)_position.Y,
                (int)Size.X, (int)Size.Y);

            _quitBtn = new Rectangle((int)_position.X, (int)_position.Y,
                (int)Size.X, (int)Size.Y);

            var mouseRectangle = new Rectangle(mouse.X, mouse.Y, 1, 1);

            CheckBtnColors(_optionsBtn, mouseRectangle, mouse);
            CheckBtnColors(_enterGameBtn, mouseRectangle, mouse);
            CheckBtnColors(_quitBtn, mouseRectangle, mouse);
        }

        public void SetPosition(Vector2 newPosition)
        {
            _position = newPosition;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            /* It does not matter which button is called here, the spritebatch just needs
             an object in order to activate. Whichever button is called here will be drawn first
             followed by the rest. */
             
            spriteBatch.Draw(_buttonTexture, _enterGameBtn, _color);
        }

        private void CheckBtnColors(Rectangle button, Rectangle mouseRectangle, MouseState mouse)
        {
            if (mouseRectangle.Intersects(button))
            {
                // Flash the red value of the button up then down, repeat.
                if (_color.A == 255) 
                    _down = false;
                if (_color.A == 0) 
                    _down = true;
                if (_down) 
                    _color.A += 3;
                else 
                    _color.A -= 3;

                // The button has been pressed
                if (mouse.LeftButton == ButtonState.Pressed) 
                    IsClicked = true;

                // The button has been released
                if (mouseRectangle.Intersects(button) && mouse.LeftButton == ButtonState.Released)
                    IsClicked = false;
            }
            else if (_color.A < 255)
            {
                _color.A += 3;
                IsClicked = false;
            }
        }
    }
}