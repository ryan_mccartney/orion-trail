﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Orion_Trail_McCartney.Main;

namespace Orion_Trail_McCartney.AsterShips
{
    class Ship
    {
        private readonly int _step;

        public int Damage;
        public Vector2 Direction;
        public readonly float Speed;
        public readonly Vector2 RotationCentre;
        public float RotationAngle;
        public Vector2 Position;
        public int Health;
        public Texture2D Texture;

        public bool Visible = true;
        
        public Ship(Texture2D texture, float x, float y, int step, int health, float speed, int damage, bool visible)
        {
            RotationCentre.X = (float)texture.Width / 2;
            RotationCentre.Y = (float)texture.Height / 2;
            _step = step;
            Speed = speed;
            Texture = texture;
            Position.X = x;
            Position.Y = y;
            Health = health;
            Damage = damage;
            Visible = visible;
        }

        public void MoveUp()
        {
            Position.Y -= _step;
        }

        public void MoveDown()
        {
            Position.Y += _step;
        }

        public void Rotate(float amount)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
                RotationAngle -= 6 * (amount);    
            
            else if (Keyboard.GetState().IsKeyDown(Keys.Right))
                RotationAngle += 6 * (amount);    
            
            
            RotationAngle = RotationAngle % (MathHelper.Pi * 4);
        }

        public void MoveInDirectionOfShip(float elapsedTime, int sign)
        {
            Direction = new Vector2((float)Math.Cos(RotationAngle), (float)Math.Sin(RotationAngle));
            Direction.Normalize();
            if (sign > 0)
                Position += Direction * Speed * elapsedTime;
            else if (sign < 0)
                Position -= Direction * Speed * elapsedTime;
        }

        public void Update(int screenWidth, int screenHeight)
        {
            if (Position.X < 0)
                Position.X = screenWidth;

            if (Position.X > screenWidth)
                Position.X = 0;

            if (Position.Y < 0)
                Position.Y = screenHeight;

            if (Position.Y > screenHeight)
                Position.Y = 0;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
                spriteBatch.Draw(Texture, Position, null, Color.White, RotationAngle, RotationCentre, 1.0f,
                    SpriteEffects.None, 0f);
        }
    }    
}