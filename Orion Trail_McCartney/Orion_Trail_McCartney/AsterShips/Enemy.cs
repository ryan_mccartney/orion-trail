using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Orion_Trail_McCartney.Main;

namespace Orion_Trail_McCartney.AsterShips
{
    class Enemy : Ship
    {
        public Enemy(Texture2D texture, float x, float y, int step, int initialHealth, float speed, int damage) 
            : base(texture, x, y, step, initialHealth, speed, damage, visible: true)
        {
            
        }

        public void Update(int screenWidth, int screenHeight, float elapsedTime, Vector2 playerPosition)
        {
            // turn enemy ship in the direction of the player ship
            RotationAngle = (float)Math.Atan((playerPosition.Y - Position.Y) / (playerPosition.X - Position.X));            
            
            // make enemy ship follow player ship
            MoveInDirectionOfShip(elapsedTime, +1);
            
            // call the update method of Ship class
            base.Update(screenWidth, screenHeight);
        }
    }
}