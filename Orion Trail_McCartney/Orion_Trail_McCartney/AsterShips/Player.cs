﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Orion_Trail_McCartney.AsterShips
{
    class Player : Ship
    {
        public Player(Texture2D texture, float x, float y, int step, int initialHealth, float speed, int damage) 
            : base(texture, x, y, step, initialHealth, speed, damage, visible: true)
        {
            
        }

        public void Update(int screenWidth, int screenHeight, float elapsedTime)
        {
            // MOVEMENT OF THE SHIP
            if (Keyboard.GetState().IsKeyDown(Keys.Up))
                MoveInDirectionOfShip(elapsedTime, +1);
            else if (Keyboard.GetState().IsKeyDown(Keys.Down))
                MoveInDirectionOfShip(elapsedTime, -1);

            // ROTATION OF THE SHIP
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
                Rotate(elapsedTime);
            else if (Keyboard.GetState().IsKeyDown(Keys.Right))
                Rotate(elapsedTime);

            // Call the update method of Ship class
            base.Update(screenWidth, screenHeight);
        }
    }
}