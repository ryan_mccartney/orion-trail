using System.Drawing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Orion_Trail_McCartney.Main;
using Orion_Trail_McCartney.Music;
using Color = Microsoft.Xna.Framework.Color;

namespace Orion_Trail_McCartney.Combat
{
    class Laser
    {
        #region Property Declarations

        // Image representing the Laser
        public Texture2D Texture;
        // Position of the Laser relative to the upper left side of the screen
        public Vector2 Position;
        // State of the Laser
        public bool Active;
        // The amount of damage the laser can inflict to an enemy
        public int Damage;
        // Represents the viewable boundary of the game
        Viewport _viewport;
        // Firerate of laser
        public double LaserFireRate;
        // Angle of shot of the laser when fired
        public float _laserShotAngle;

        #endregion

        // Get the width of the laser ship
        public int Width
        {
            get { return Texture.Width; }
        }

        // Get the height of the laser ship
        public int Height
        {
            get { return Texture.Height; }
        }

        public void Initialize(Viewport viewport, Texture2D texture, Vector2 position)
        {
            Texture = texture;
            Position = position;
            _viewport = viewport;
            Active = true;
            Damage = 2;
            LaserFireRate = 500;
            _laserShotAngle = 0;
        }

        public void Update(Vector2 direction)
        {
            Position += direction;

            // Deactivate the bullet if it goes out of screen
            if (Position.X + Texture.Width / 2 > _viewport.Width
                || Position.X < 0
                || Position.Y + Texture.Height / 2 > _viewport.Height
                || Position.Y < 0)
                Active = false;
        }

        public void Draw(SpriteBatch spriteBatch, float angle)
        {
            spriteBatch.Draw(Texture, Position, null, Color.White, angle, 
                new Vector2(Width / 2, Height / 2), 1f, SpriteEffects.None, 0f);
        }

        public void RandomLaserEffect()
        {
            Sfx.PlayLaser();
        }
    }
}
