using Orion_Trail_McCartney.Main;

namespace Orion_Trail_McCartney.UI
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (OrionTrailGame game = new OrionTrailGame())
            {
                game.Run();
            }
        }
    }
#endif
}

