#region Ruminate Bibliography
/* ----------------------------- /
 * The RuminateGUI Framework is used for some
 * of the screen elements (Mostly labeled Rum<element>).
 * http://xnagui.codeplex.com/documentation
 * ----------------------------- */
#endregion
#region References

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Data.SqlTypes;
using System.Drawing;
using System.IO;
using System.IO.IsolatedStorage;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Content;
using Orion_Trail_McCartney.AsterShips;
using Orion_Trail_McCartney.Combat;
using Orion_Trail_McCartney.Music;
using Orion_Trail_McCartney.UI.RumUI;
using Ruminate.Utils;
using Button = Orion_Trail_McCartney.UI.Button;
using Color = Microsoft.Xna.Framework.Color;
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Point = Microsoft.Xna.Framework.Point;
using Rectangle = Microsoft.Xna.Framework.Rectangle;
using Screen = Orion_Trail_McCartney.UI.RumUI.Screen;

#endregion

namespace Orion_Trail_McCartney.Main
{
    // This is the main class type for the game
    public class OrionTrailGame : Game
    {
        #region GAME WORLD Variable Declarations

        public readonly GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        // High Score Variables
        // SAVING VARIABLES
        public string HighScoresFilename = "highscores.sav";

        public string SavesFilePath = (Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)) +
                                        @"\SavedGames\Orion_Trail_McCartney\OrionTrail-Saves\Player1\highscores.sav";

        private string saveNames;
        private string saveTimes;

        public StorageContainer StorageContainer;
        public StorageDevice StorageDevice;

        public IAsyncResult asyncResult;

        public enum SavingState
        {
            NotSaving,
            ReadyToSelectStorageDevice,
            SelectingStorageDevice,

            ReadyToOpenStorageContainer,
            OpeningStorageContainer,
            ReadyToSave
        }

        SavingState savingState = SavingState.NotSaving;

        // SCREEN BOUNDARIES
        private float _aspectRatio;
        public Point OldWindowSize;
        private RenderTarget2D _offScreenRenderTarget;

        // KEYBOARD STATES FOR BUTTON PRESSES
        private KeyboardState _oldKeyboardState;
        
        // ENUM FOR GAME STATES - ACTIVE SCREENS
        public enum GameState
        {
            MainMenu,
            Options,
            Asteroids,
            Quit,
            AfterDeath,
            HighScores
        }

        // Loads the Main Menu first by default
        // Gamestate variable used for maintaining current gamestate after a
        // gamestate change.
        public static GameState CurrentGameState = GameState.MainMenu;

        // Main Menu Buttons
        private Button _btnPlay;
        private Button _btnOptions;
        private Button _btnQuit;

        // random number to use for spawning enemies, etc
        public static Random Random;

        // enemies
        private readonly List<Enemy> _enemies;
        private const int MaxEnemies = 3;
        private readonly List<Texture2D> _enemyTextures;
        
        // player
        private Player _player;
        private PlayerHealthBar _playerHealthBar;

        private Texture2D _background;

        // Explosion graphics list
        Texture2D _explosionTexture;
        List<Animation> _explosions;
        
        // Initialising Laser Texture and Lists
        Texture2D _laserTexture;
        List<Laser> _lasers;
        private Laser _laser;
        private Vector2 _laserDirection;
        // Timer for laser firing
        private double _laserTimer = 200;
        private const double TimerLaserFireRate = 200;

        // Audio
        public List<Song> MainMusic;

        public static AudioEngine Audio;
        public static SoundBank Sounds;
        public static WaveBank Waves;
        public static AudioCategory MusicCategory;
        public static AudioCategory SfxCategory;

        // TIMER REQUIREMENTS
        private int _counter = 1;
        private const int Limit = 999;
        private const float CountDuration = 1f; // Every 1 second
        private float _currentTime;
        private int Playlevel = 1;

        private int _survivalCounter = 1;
        private float _currentSurvivalTime;
        private const int SurvivalLimit = 999;

        #region RuminateGUI
        Screen _currentScreen;
        Screen[] _currentScreens;
        int _screensIndex;
        public bool OptionsSettingsChanged = false;

        public SpriteFont RumSpriteFont;
        public Texture2D RumImageMap;
        public string RumMap;

        public SpriteFont GreySpriteFont;
        public Texture2D GreyImageMap;
        public string GreyMap;
        public List<Texture2D> RumImages; 
        #endregion

        #endregion

        public OrionTrailGame()
        {
            _graphics = new GraphicsDeviceManager(this)
            {
                PreferredDepthStencilFormat = DepthFormat.Depth24Stencil8
            };
            Content.RootDirectory = "Content";

            Random = new Random();
            _enemies = new List<Enemy>();
            _enemyTextures = new List<Texture2D>();
            RumImages = new List<Texture2D>();
            MainMusic = new List<Song>();

            Window.AllowUserResizing = true;
            Window.ClientSizeChanged += delegate
            {
                if (_currentScreen != null)
                {
                    // Resizes all RumUI elements to scale with current resolution
                    _currentScreen.OnResize();
                }

            };
            // Calling event handler for manual resizing of the application window
            Window.ClientSizeChanged += new EventHandler<EventArgs>(Window_ClientSizeChanged);
            

            // SETTING SCREEN RESOLUTION AND FULLSCREEN SETTINGS
            _graphics.PreferredBackBufferWidth = 1024;//GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            _graphics.PreferredBackBufferHeight = 650;//GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            _graphics.IsFullScreen = false;
            
            // Would recommend not setting .IsFullScreen to true. it can cause some pretty intense
            // graphical errors (ie. runtime errors and horrible UI glitches).
        }

        #region Content Loading -- Content, Initialize, Songs, Buttons, Textures, etc.
        // LoadContent will be called once per game and is the place to load
        // all of your content.
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            Viewport viewport = _graphics.GraphicsDevice.Viewport;

            // Setting Screen Boundaries and initial Window Size
            _aspectRatio = GraphicsDevice.Viewport.AspectRatio;
            OldWindowSize = new Point(Window.ClientBounds.Width, Window.ClientBounds.Height);

            // Creating a render target that will update the aspect ratios of all graphics.
            // This is used to maintain the size of graphics when the window is resized.
            _offScreenRenderTarget = new RenderTarget2D(GraphicsDevice, Window.ClientBounds.Width,
                Window.ClientBounds.Height);

            // Loading Player Values
            const int step = 5;
            const int initHealth = 1000;
            const int speed = 1000;
            const int damage = 2;

            _player = new Player(Content.Load<Texture2D>("Ships/Player/mainShip"), 
                (float)viewport.Width / 2, (float)viewport.Height / 2, step, initHealth, speed, damage);

            /* Init PlayerHealthBar Object
                Assign location - constants due to location of elements
                in RumCombatHud Screen */
            _playerHealthBar = new PlayerHealthBar(this, new Rectangle(40, 12, 300, 16));
            _playerHealthBar.minimum = 0;
            _playerHealthBar.maximum = 1000;

            // Initialize the explosion list
            _explosionTexture = Content.Load<Texture2D>("Animations/explosion");

            _laserTexture = Content.Load<Texture2D>("Animations/laser");

            _background = Content.Load<Texture2D>("Backgrounds/background");

            // MOUSE
            IsMouseVisible = true;

            // Calling loading subs
            LoadSongs();
            LoadButtons();
            LoadEnemyTextures();
            LoadRumUi();
            LoadRumImages();
        }

        protected override void UnloadContent()
        {
            // Disposes of render targets and textures when window is resized
            if (_offScreenRenderTarget != null)
                _offScreenRenderTarget.Dispose();

            if (_spriteBatch != null)
                _spriteBatch.Dispose();

        }

        protected override void Initialize()
        {
            // RUMUI SCREENS LIST
            _currentScreens = new Screen[]
            {
                new RumClearScreen(),
                new RumOptionsMenu(),
                new RumDeathScreen(),
                new RumCombatHud(),
                new RumMainMenuScreen(),
            };

            // Initializing all lists
            _explosions = new List<Animation>();
            _lasers = new List<Laser>();
            IsMouseVisible = true;

            // SAVING HIGHSCORES
            // Check for a existing savefile
            if (File.Exists((Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)) +
                                    @"\SavedGames\Orion_Trail_McCartney\OrionTrail-Saves\Player1\highscores.sav"))
            {
                // If found, update the highscores to be that of the values in save file
                GetHighScores();
            }
            else
            {
                // If not found, create dummy value and file and update current scores
                saveNames = saveGameData.PlayerName;
                saveTimes = saveGameData.survivalTime.ToString(CultureInfo.InvariantCulture);

                CreateSaveData();
                ManualSaveStart();
            }

            base.Initialize();
        }

        public void LoadRumUi()
        {
            GreyImageMap = Content.Load<Texture2D>(@"Menus\RuminateSkins\GreySkin\ImageMap");
            GreyMap = File.OpenText(@"Content\Menus\RuminateSkins\GreySkin\Map.txt").ReadToEnd();
            GreySpriteFont = Content.Load<SpriteFont>(@"Menus\RuminateSkins\GreySkin\Font");

            RumImageMap = Content.Load<Texture2D>(@"Menus\RuminateSkins\TestSkin\ImageMap");
            RumMap = File.OpenText(@"Content\Menus\RuminateSkins\TestSkin\Map.txt").ReadToEnd();
            RumSpriteFont = Content.Load<SpriteFont>(@"Menus\RuminateSkins\TestSkin\Font");

            DebugUtils.Init(_graphics.GraphicsDevice, GreySpriteFont);

            _screensIndex = 0;
            _currentScreen = _currentScreens[_screensIndex];
            _currentScreen.Init(this, GraphicsDevice);
        }

        public void LoadRumImages()
        {
            string name;
            string[] files = Directory.GetFiles("Content/Menus/RumImages");
            for (int i = 0; i < files.Length; i++)
            {
                name = files[i].Remove(0, "Content/".Length).Replace(".xnb", "");
                RumImages.Add(Content.Load<Texture2D>(name));
            }    
        }

        public void LoadSongs()
        {
            
            Audio = new AudioEngine(@"Content\Audio\OrionTrail_XACT.xgs");
            Sounds = new SoundBank(Audio, @"Content\Audio\Sound Bank1.xsb");
            Waves = new WaveBank(Audio, @"Content\Audio\Wave Bank1.xwb");
            

            SoundTrack.Initialize();
            Sfx.Initialize();

        }

        private void LoadButtons()
        {
            // Setting Main Menu Buttons

            #region Button Addition Instructions

            /* For each new button that is ontop of each other, add 88 to the previous button's height.
             * Eg. _graphics.PreferredBackbufferHeight / 2) + 88));
             */

            #endregion

            _btnPlay = new Button(Content.Load<Texture2D>("Buttons/PlayBtn"));
            _btnPlay.SetPosition(new Vector2((Window.ClientBounds.Width/2) - (250/2),
                ((float)Window.ClientBounds.Height / 2)));

            _btnOptions = new Button(Content.Load<Texture2D>("Buttons/OptionsBtn"));
            _btnOptions.SetPosition(new Vector2((Window.ClientBounds.Width / 2) - (250 / 2),
                (Window.ClientBounds.Height / 2) + 88));

            _btnQuit = new Button(Content.Load<Texture2D>("Buttons/QuitBtn"));
            _btnQuit.SetPosition(new Vector2((Window.ClientBounds.Width / 2) - (250 / 2),
                (Window.ClientBounds.Height / 2) + 176));
        }

        public void LoadEnemyTextures()
        {
            string name;
            string[] files = Directory.GetFiles("Content/Ships/Enemy");
            for (int i = 0; i < files.Length; i++)
            {
                name = files[i].Remove(0, "Content/".Length).Replace(".xnb", "");
                _enemyTextures.Add(Content.Load<Texture2D>(name));
            }
        }

        #endregion

        #region Explosions

        private void AddExplosion(Vector2 position)
        {
            Animation explosion = new Animation();
            explosion.Initialize(_explosionTexture, position, 134, 134, 12, 45, Color.White, 1f, false);
            _explosions.Add(explosion);
        }

        #endregion

        #region Lasers
        private void Addlaser(Vector2 position)
        {
            Laser laser = new Laser();
            laser.Initialize(GraphicsDevice.Viewport, _laserTexture, position);
            laser._laserShotAngle = _player.RotationAngle;

            _lasers.Add(laser);

        }
        
        private void UpdateLasers(float elapsed)
        {
            // Update the lasers
            for (int i = _lasers.Count - 1; i >= 0; i--)
            {

                _laserDirection = new Vector2((float)Math.Cos(_lasers[i]._laserShotAngle), (float)Math.Sin(_lasers[i]._laserShotAngle));
                _laserDirection.Normalize();
                

                _lasers[i].Update(_player.Speed*elapsed*_laserDirection);

                if (_lasers[i].Active == false)
                {
                    _lasers.RemoveAt(i);
                }
            }
        }

        #endregion

        #region Highscores & Saving
        
        public struct SaveGameData
        {
            // Struct for save data
            public string PlayerName;
            public int survivalTime;
        }

        private SaveGameData saveGameData = new SaveGameData()
        {
            // Create Placeholder data to initialize struct
            // This is overwritten later, stored so that null exceptions don't
            // occur and end runtime - There's a purpose other than an SDD joke, I swear!
            PlayerName = "Howse",
            survivalTime = 10

        };

        private void UpdateSaving()
        {
            switch (savingState)
            {
                case SavingState.ReadyToSelectStorageDevice:
                    {
                        // Create storage device
                        asyncResult = StorageDevice.BeginShowSelector(PlayerIndex.One, null, null);
                        savingState = SavingState.SelectingStorageDevice;
                    }
                    break;

                case SavingState.SelectingStorageDevice:
                    // Select the created storage device
                    if (asyncResult.IsCompleted)
                    {
                        StorageDevice = StorageDevice.EndShowSelector(asyncResult);
                        savingState = SavingState.ReadyToOpenStorageContainer;
                    }
                    break;

                case SavingState.ReadyToOpenStorageContainer:
                    // Open created storage device and create sub-folder
                    if (StorageDevice == null || !StorageDevice.IsConnected)
                    {
                        savingState = SavingState.ReadyToSelectStorageDevice;
                    }
                    else
                    {
                        asyncResult = StorageDevice.BeginOpenContainer("OrionTrail-Saves", null, null);
                        savingState = SavingState.OpeningStorageContainer;
                    }
                    break;

                case SavingState.OpeningStorageContainer:
                    // Open created sub folder
                    if (asyncResult.IsCompleted)
                    {
                        StorageContainer = StorageDevice.EndOpenContainer(asyncResult);
                        savingState = SavingState.ReadyToSave;
                    }
                    break;

                case SavingState.ReadyToSave:
                    // Save to subfolder
                    if (StorageContainer == null)
                    {
                        savingState = SavingState.ReadyToOpenStorageContainer;
                    }
                    else
                    {
                        try
                        {
                            DeleteExisting();
                            Save();
                        }
                        catch (IOException e)
                        {
                            // Replace with in game dialog notifying user of error
                            Debug.WriteLine(e.Message);
                        }
                        finally
                        {
                            StorageContainer.Dispose();
                            StorageContainer = null;
                            savingState = SavingState.NotSaving;
                        }
                    }
                    break;
            }
        }

        private void DeleteExisting()
        {
            // Overwrite current savefile
            if (StorageContainer.FileExists(HighScoresFilename))
            {
                StorageContainer.DeleteFile(HighScoresFilename);
            }
        }

        private void Save()
        {
            // Write the save file
            using (Stream stream = StorageContainer.CreateFile(HighScoresFilename))
            {
                XmlSerializer serializer = new XmlSerializer(typeof (SaveGameData));
                serializer.Serialize(stream, saveGameData);
            }
            
        }

        private void UpdateSaveKey(Keys saveKey)
        {
            int prevSaveTime = Convert.ToInt32(saveTimes);

            // Quicksave key to start save process
            if (Keyboard.GetState().IsKeyDown(saveKey))
            {
                if (savingState == SavingState.NotSaving && _currentSurvivalTime >= prevSaveTime)
                {
                    CreateSaveData();
                    savingState = SavingState.ReadyToOpenStorageContainer;
                }
            }
        }

        private void ManualSaveStart()
        {
            int prevSaveTime = Convert.ToInt32(saveTimes);

            // Manually start the saving process
            if (savingState == SavingState.NotSaving && _currentSurvivalTime >= prevSaveTime)
            {
                CreateSaveData();
                savingState = SavingState.ReadyToOpenStorageContainer;
            }
        }

        private void CreateSaveData()
        {
            // Update savedata to be current
            saveGameData = new SaveGameData()
            {
                PlayerName = "Player 1",
                survivalTime = (int) _currentSurvivalTime

            };
        }

        public void GetHighScores()
        {
            // Retrieve xml doc, then read in the two elements we need
            XmlDocument doc = new XmlDocument();

            // Load in the file from MyDocuments (dynamic to each computer's filepaths)
            doc.Load((Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)) + @"\SavedGames\Orion_Trail_McCartney\OrionTrail-Saves\Player1\highscores.sav");
            
            // Split the xml nodes into child nodes of attribute "SaveGameData"
            XmlNodeList elemList = doc.GetElementsByTagName("SaveGameData");

            for (int i = 0; i < elemList.Count; i++)
            {
                if (elemList[i] != null)
                {
                    // Constant use as there is only every two child nodes for "SaveGameData"
                    saveNames = elemList[i].ChildNodes[0].InnerText;
                    saveTimes = elemList[i].ChildNodes[1].InnerText;
                }
            }
        }
        #endregion

        public void UpdatePlayerHealth()
        {
            _playerHealthBar.value = _player.Health;
        }

        public void Die()
        {
            // Create explosion on Player's death position
            AddExplosion(_player.Position);

            // Remove the texture of Player ship upon death
            _player.Visible = false;

            // Bring up the playerdeath HUD
            ChangeRumScreen(2);

            // If in survival mode save the player's time then reset currentTime
            if (RumMainMenuScreen._survivalModeCheckBox.IsToggled)
            {
                ManualSaveStart();
                _currentSurvivalTime = 1;
            }

        }

        private void Window_ClientSizeChanged(object sender, EventArgs e)
        {
            // Remove event handler to stop it being called again during this subroutine
            Window.ClientSizeChanged -= new EventHandler<EventArgs>(Window_ClientSizeChanged);
            
            /* The minimum resolution is 1024*650 (By default). There can be serious graphical issues when the game
             * runs lower than this resolution and so the first two if statements are to catch this and force the window back to
             * default minimum size.
             * 
             * The second are if the window goes too large, then it resizes back to default. The game can be maximised by clicking on the
             * maximise button default to all windows applications, but manually resizing the window to be larger by clicking and dragging the edges
             * causes some major graphical issues. This method is to prevent these issues while still allowing some level of window resizing (maximise window)
             */

            if (Window.ClientBounds.Width < 1024)
            {
                _graphics.PreferredBackBufferWidth = 1024;
                _graphics.PreferredBackBufferHeight = 650;
            }
            else if (Window.ClientBounds.Height < 650)
            {
                _graphics.PreferredBackBufferWidth = 1024;
                _graphics.PreferredBackBufferHeight = 650; 
            }

            else if (Window.ClientBounds.Width > 1024)
            {
                _graphics.PreferredBackBufferWidth = 1024;
                _graphics.PreferredBackBufferHeight = 650;   
            }

            else if (Window.ClientBounds.Height > 650)
            {
                _graphics.PreferredBackBufferWidth = 1024;
                _graphics.PreferredBackBufferHeight = 650;
            }

            // Updating the rumUI
            int oldIndex;

            if (_screensIndex != 0)
            {
                oldIndex = _screensIndex;
                ChangeRumScreen(0);
                _screensIndex = oldIndex;
            }

            // Apply Changes
            _graphics.ApplyChanges();

            ChangeRumScreen(_screensIndex);

            // Update the old window size
            OldWindowSize = new Point(Window.ClientBounds.Width, Window.ClientBounds.Height);

            // Add the event handler back
            Window.ClientSizeChanged += new EventHandler<EventArgs>(Window_ClientSizeChanged);

        }

        public void UpdateCountDown(GameTime gameTime)
        {
            if (_currentTime > 0)
            {
                _currentTime -= (float)gameTime.ElapsedGameTime.TotalSeconds;
            }

            if (_currentTime >= CountDuration)
            {
                _counter++;
                RumCombatHud._timeLeftLabel.Value = "Time Left: " + _currentTime.ToString("## 'sec'");
            }

            if (_counter >= Limit)
            {
                _counter = 0; //Reset counter
            }

            if (_currentTime < 0 && _player.Health > 0)
            {
                Playlevel++;
                _currentTime = Random.Next(15, 30*Playlevel);
            }
        }

        public void SurvivalTimer(GameTime gameTime)
        {
            if (_currentSurvivalTime >= 0)
            {
                _currentSurvivalTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
            }

            if (_currentSurvivalTime >= CountDuration)
            {
                _survivalCounter++;
                RumCombatHud._timeLeftLabel.Value = "Survived: " + _currentSurvivalTime.ToString("## 'sec'");
            }

            if (_survivalCounter >= SurvivalLimit)
            {
                _survivalCounter = 0; //Reset counter
            }

            if (_currentSurvivalTime < 0)
                _currentSurvivalTime = 1;

        }

        // Allows the game to run logic such as updating the world, checking for collisions, gathering input, and playing audio.
        protected override void Update(GameTime gameTime)
        {
            MouseState mouse = Mouse.GetState();
            var elapsedMilliSec = (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            _laserTimer -= elapsedMilliSec;
            var newState = Keyboard.GetState();

            UpdateGameState();
            ManualUpdateRumScreen();

            UpdateSaveKey(Keys.F1);
            UpdateSaving();

            #region Asteroids Gamestate Events
            if (CurrentGameState == GameState.Asteroids)
            {
                
                if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                    CurrentGameState = GameState.MainMenu;

                if (newState.IsKeyDown(Keys.Space) && _laserTimer < 0)
                {                   
                    Addlaser(_player.Position + new Vector2(_player.Texture.Width / 2, -5));

                    _laser = new Laser();
                    _laser.RandomLaserEffect();
                    
                    _laser._laserShotAngle = _player.RotationAngle;
                    
                    _laserTimer = TimerLaserFireRate; // Resetting laserTimer to a const

                }

                // Old state is used to stop constant keypressing.
                _oldKeyboardState = newState;

                _player.Update(Window.ClientBounds.Width, Window.ClientBounds.Height, (float)gameTime.ElapsedGameTime.TotalSeconds);
                _playerHealthBar.Update(gameTime);
                UpdateEnemies((float) gameTime.ElapsedGameTime.TotalSeconds);
                UpdateLasers((float) gameTime.ElapsedGameTime.TotalSeconds);
                UpdateCollision();
                UpdateLaserHits();
                UpdateExplosions(gameTime);

                if (RumMainMenuScreen._survivalModeCheckBox.IsToggled)
                    SurvivalTimer(gameTime);
                else
                    UpdateCountDown(gameTime); 
                

                

            }
            #endregion

            #region Options Gamestate Events
            if (CurrentGameState == GameState.Options)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                    CurrentGameState = GameState.MainMenu;
                 
            }
            #endregion

            #region Audio Updates

            if (Audio != null)
                Audio.Update();

            if (Sounds != null)
                SoundTrack.Update();

            #endregion


            base.Update(gameTime);
        }

        /* The method handles the updating of static values that
           would normally be protected by encapsulation but in order to make the UI
           functional cannot be. */
        private void ManualUpdateRumScreen()
        {
            var newState = Keyboard.GetState();

            #region Option's Slider Value Saving

            if (_currentScreen == _currentScreens[1]
                && RumOptionsMenu.VolSlider.Value != 100
                && RumOptionsMenu.VolSlider.Value == 0
                && OptionsSettingsChanged == false)
            {
                RumOptionsMenu.SetSliderValues(100);
            }
            else if (_currentScreen == _currentScreens[1]
                && OptionsSettingsChanged)
            {
                RumOptionsMenu.VolSlider.Value = RumOptionsMenu.CurrentVolValue;
            }

            #endregion

            _currentScreen.Update();

            _oldKeyboardState = newState;
        }

        private void ChangeRumScreen(int curScreen)
        {
            _screensIndex = curScreen;
            _currentScreen = _currentScreens[_screensIndex];
            _currentScreen.Init(this, GraphicsDevice);


            _currentScreen.Update();
        }

        private void UpdateGameState()
        {
            MouseState mouse = Mouse.GetState();

            // Update each button here to make visible
            // THIS is the section that the instructions in button.cs refer to.

            switch (CurrentGameState)
            {
                case GameState.MainMenu:
                    // Conditions for after the player is dead for being able to re-enter the game
                    if (_btnPlay.IsClicked && _player.Visible)
                    {
                        CurrentGameState = GameState.Asteroids;
                    }

                    else if (_btnPlay.IsClicked && !_player.Visible)
                    {
                        CurrentGameState = GameState.Asteroids;
                    }

                    if (_btnOptions.IsClicked)
                        CurrentGameState = GameState.Options;

                    if (_btnQuit.IsClicked)
                        Exit();

                    _btnPlay.Update(mouse);
                    _btnOptions.Update(mouse);
                    _btnQuit.Update(mouse);

                    if (_screensIndex != 4)
                        ChangeRumScreen(4);

                    break;

                case GameState.Asteroids:

                    if (_screensIndex != 3 && _player.Visible)
                        ChangeRumScreen(3);

                    if (!RumMainMenuScreen._survivalModeCheckBox.IsToggled)
                    {
                        if (_currentTime <= 0)
                        {
                            _currentTime = Random.Next(15, 30);
                        }
                    }
                    else
                    {
                        if (_currentSurvivalTime <= 0)
                            _currentSurvivalTime = 1;
                    }
                    break;

                case GameState.Options:
                    // False .IsCLicked stops the button from 'double clicking'
                    _btnOptions.IsClicked = false;

                    if (_screensIndex != 1)
                        ChangeRumScreen(1);
                   
                    break;

                case GameState.Quit:
                    // This Exit() method cannot be called anywhere but the Main Game Class (this class)
                    // Calling this gamestate just quits the game, and as gamestates may be changed in other classes
                    // this gamestate is used for exiting the game from user button input. 
                    Exit();
                    break;

                case GameState.AfterDeath:
                    // This enum is for after a player dies and resets most of the game.
                    _btnPlay.IsClicked = false;

                    // Remove all enemies
                    _enemies.Clear();

                    // Reset player
                    _player.Visible = true;
                    _player.Health = 1000;
                    _player.Position = new Vector2((float)_graphics.GraphicsDevice.Viewport.Width / 2,
                       (float)_graphics.GraphicsDevice.Viewport.Height / 2);
                    Playlevel = 1;

                    // Reset timers
                    if (!RumMainMenuScreen._survivalModeCheckBox.IsToggled)
                        _currentTime = Random.Next(15, 30);
                    else
                        _currentSurvivalTime = 1;
                    
                    // Update healthbar
                    UpdatePlayerHealth();
                    
                    // Restart gamestate
                    CurrentGameState = GameState.Asteroids;

                    break;

                case GameState.HighScores:

                    // Check if there's a save file
                    if (File.Exists((Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)) +
                                    @"\SavedGames\Orion_Trail_McCartney\OrionTrail-Saves\Player1\highscores.sav"))
                    {
                        // Check that the most recent survival time is smaller than the best time
                        if (_currentSurvivalTime <= saveGameData.survivalTime)
                        {
                            GetHighScores();
                        }
                    }
                    // If not, populate a new file with placeholder data
                    else
                    {
                        saveNames = saveGameData.PlayerName;
                        saveTimes = saveGameData.survivalTime.ToString(CultureInfo.InvariantCulture);

                        // Create file and then update the file to screen
                        CreateSaveData();
                        ManualSaveStart();
                    }

                    break;

            }
        }

        private void UpdateEnemies(float gameTime)
        {
            int x = 0, y = 0;
            // make new enemies if there are less than the maximum allowed
            if (_enemies.Count < MaxEnemies)
            {
                int side = Random.Next(1, 3);
                if (side == 1)
                {
                    x = 0; // left side
                    y = Random.Next(Window.ClientBounds.Height);
                }
                else if (side == 2)
                {
                    x = Window.ClientBounds.Width; // right side
                    y = Random.Next(Window.ClientBounds.Height);
                }
                
                int step = 3;
                int initHealth = 5;
                int speed = 200;
                int damage = Random.Next(10, 100);
                _enemies.Add(new Enemy(_enemyTextures[Random.Next(_enemyTextures.Count)], x, y, step, initHealth, speed, damage));
            }

            // If the player isn't dead, move the enemies about
            if (_player.Visible)
            {
                foreach (var enemy in _enemies)
                    enemy.Update(Window.ClientBounds.Width, Window.ClientBounds.Height, gameTime, _player.Position);
            }
        }

        private void UpdateCollision()
        {
            // Use the Rectangle's built-in intersect function to determine if two objects are overlapping

            // Only create the rectangle once for the player
            Rectangle playerRectangle = new Rectangle((int)_player.Position.X,
                (int)_player.Position.Y, _player.Texture.Width, _player.Texture.Height);

            Rectangle enemyRectangle;

            // Do the collision between the player and the enemies
            for (var i = 0; i < _enemies.Count; i++)
            {
                enemyRectangle = new Rectangle((int)_enemies[i].Position.X, (int)_enemies[i].Position.Y,
                    _enemies[i].Texture.Width, _enemies[i].Texture.Height);
                if (playerRectangle.Intersects(enemyRectangle))
                {
                    // Subtract the health from the player based on the enemy damage
                    if (_player.Health > 0)
                    {
                        if (_player.Health - _enemies[i].Damage >= 0)
                        {
                            _player.Health -= _enemies[i].Damage;
                        }
                        else
                        {
                            _player.Health = 0;
                        }
                    }

                    // Since the enemy collided with the player destroy it
                    AddExplosion(_enemies[i].Position);

                    // Play the explosion sound
                    Sfx.PlayExplosions();

                    _enemies.Remove(_enemies[i]);
                    // If the player health is less than zero we die
                    if (_player.Health <= 0)
                        Die();
                }
            }
        }

        private void UpdateLaserHits()
        {
            Rectangle laserRectangle;

            Rectangle enemyRectangle;

            // Do the collision between the lasers and the enemies

            for (var j = 0; j < _lasers.Count; j++)
                for (var i = 0; i < _enemies.Count; i++)
                {
                    laserRectangle = new Rectangle((int)_lasers[j].Position.X, (int)_lasers[j].Position.Y,
                        _lasers[j].Texture.Width, _lasers[j].Texture.Height);
                    enemyRectangle = new Rectangle((int)_enemies[i].Position.X, (int)_enemies[i].Position.Y,  
                        _enemies[i].Texture.Width, _enemies[i].Texture.Height);
                    if (laserRectangle.Intersects(enemyRectangle))
                    {
                        // Since the enemy collided with the player destroy it
                        AddExplosion(_enemies[i].Position);

                        // Play the explosion sound
                        Sfx.PlayExplosions();

                        _enemies.Remove(_enemies[i]);

                        _lasers[j].Active = false;

                        // If the player health is less than zero we die
                        if (_player.Health <= 0)
                            Die();
                    }
                }
        }

        private void UpdateExplosions(GameTime gameTime)
        {
            for (int i = _explosions.Count - 1; i >= 0; i--)
            {
                _explosions[i].Update(gameTime);
                if (_explosions[i].Active == false)
                {
                    _explosions.RemoveAt(i);
                }
            }
        }

        private void DrawGameState()
        {
            // GameState Switch
            switch (CurrentGameState)
            {
                // _spritebatch.Draw with a content load is loading the background 
                // <buttonName>.Draw is loading the button

                case GameState.Options:
                    _spriteBatch.Draw(Content.Load<Texture2D>("Backgrounds/titleScreen"),
                        new Rectangle(0, 0, _offScreenRenderTarget.Width, _offScreenRenderTarget.Height), Color.White);
                    break;

                case GameState.MainMenu:
                    _spriteBatch.Draw(Content.Load<Texture2D>("Backgrounds/titleScreen"),
                        new Rectangle(0, 0, _offScreenRenderTarget.Width, _offScreenRenderTarget.Height), Color.White);
                    _btnOptions.Draw(_spriteBatch);
                    _btnPlay.Draw(_spriteBatch);
                    _btnQuit.Draw(_spriteBatch);
                    break;

                case GameState.Asteroids:
                    UpdatePlayerHealth();
                    _playerHealthBar.Draw(_spriteBatch);

                    if (!_player.Visible && !RumMainMenuScreen._survivalModeCheckBox.IsToggled)
                        _spriteBatch.DrawString(GreySpriteFont, "You reached level: " + Playlevel + "!",
                                    new Vector2((Window.ClientBounds.Width / 2) - 130, (Window.ClientBounds.Height / 2) - 50),
                                    new Color(222, 238, 214));
                    else
                    {
                        if (!RumMainMenuScreen._survivalModeCheckBox.IsToggled)
                        {
                            _spriteBatch.DrawString(GreySpriteFont, "Level: " + Playlevel,
                                    new Vector2((Window.ClientBounds.Width) - 145, 32),
                                    new Color(222, 238, 214));
                        }
                    }
                    break;

                case GameState.AfterDeath:
                    break;

                case GameState.HighScores:
                    _spriteBatch.Draw(Content.Load<Texture2D>("Backgrounds/titleScreen"),
                        new Rectangle(0, 0, _offScreenRenderTarget.Width, _offScreenRenderTarget.Height), Color.White);

                    if (saveNames != null && saveTimes != null)
                    {
                        _spriteBatch.DrawString(GreySpriteFont, saveNames, new Vector2((Window.ClientBounds.Width / 2) - 80, 300), new Color(222, 238, 214));
                        _spriteBatch.DrawString(GreySpriteFont, saveTimes + " Second(s)", new Vector2((Window.ClientBounds.Width / 2) - 80, 330), new Color(222, 238, 214));
                    }
                    
                    break;
            }
        }

        // DRAW GAMETIME
        protected override bool BeginDraw()
        {
            // Begin the drawing of the screen's rendertarget for window resizing
            GraphicsDevice.SetRenderTarget(_offScreenRenderTarget);
            return base.BeginDraw();
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);
            _spriteBatch.Begin();

            _spriteBatch.Draw(_background, new Rectangle(0, 0, _graphics.PreferredBackBufferWidth, _graphics.PreferredBackBufferHeight), Color.White);

            foreach (var enemy in _enemies)
                enemy.Draw(_spriteBatch);

            if (_player.Visible)
            {
                _player.Draw(_spriteBatch);
            }

            foreach (Animation explosion in _explosions)
                explosion.Draw(_spriteBatch);

            foreach (Laser laser in _lasers)
                laser.Draw(_spriteBatch, _laser._laserShotAngle);

            DrawGameState();

            _spriteBatch.End();

            _currentScreen.Draw();
            base.Draw(gameTime);
        }

        protected override void EndDraw()
        {
            // Resetting drawing of render targets after window resize
            GraphicsDevice.SetRenderTarget(null);
            _spriteBatch.Begin();

            _spriteBatch.Draw(_offScreenRenderTarget, GraphicsDevice.Viewport.Bounds, Color.White);

            DrawGameState();
            
            _spriteBatch.End();

            _currentScreen.Draw();
            base.EndDraw();
        }

    }
}